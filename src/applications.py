# applications.py
#
# Port made from Flatseal applications.js
# by Martin Abente Lahaye
#

from datetime import date
from gi.repository import GObject, GLib, Gio, AppStreamGlib
from .info import FlatpakInfoModel

class FlatpakApplicationsModel(GObject.Object):
	_paths = None
	_info = None

	def __init__(self):
		super().__init__()
		self._info = FlatpakInfoModel()

	def _getSystemPath(self):
		systemPath = GLib.getenv('FLATPAK_SYSTEM_DIR')
		if systemPath:
			return systemPath

		return GLib.build_filenamev([GLib.DIR_SEPARATOR_S, 'var', 'lib', 'flatpak'])


	def _getUserPath(self):
		userPath = GLib.getenv('FLATPAK_USER_DIR')
		if userPath:
			return userPath

		userDataDir = GLib.get_user_data_dir()
		if self._info.getVersion():
			userDataDir = GLib.getenv('HOST_XDG_DATA_HOME')

		if userDataDir is None:
			userDataDir = GLib.build_filenamev([GLib.get_home_dir(), '.local', 'share'])

		return GLib.build_filenamev([userDataDir, 'flatpak'])


	def _getConfigPath(self):
		configPath = GLib.getenv('FLATPAK_CONFIG_DIR')
		if configPath:
			return configPath

		configPath = GLib.build_filenamev([GLib.DIR_SEPARATOR_S, 'etc', 'flatpak'])
		if self._info.getVersion():
			configPath = GLib.build_filenamev([GLib.DIR_SEPARATOR_S, 'run', 'host', 'etc', 'flatpak'])

		return configPath


	def _parseCustomInstallation(self, path):
		installations = []

		keyFile = GLib.KeyFile()
		keyFile.load_from_file(path, GLib.KeyFileFlags.NONE)

		groups = keyFile.get_groups()
		for group in groups:
			installation = {}

			try:
				installation['path'] = keyFile.get_value(group, 'Path')
			except:
				return

			try:
				installation['priority'] = keyFile.get_value(group, 'Priority')
			except:
				installation['priority'] = 0

			installations.append(installation)

		return installations


	def _getCustomInstallationsPaths(self):
		installations = []

		configPath = GLib.build_filenamev([self._getConfigPath(), 'installations.d'])

		if GLib.access(configPath, 0) != 0:
			return installations;

		directory = Gio.File.new_for_path(configPath)
		enumerator = directory.enumerate_children('*', Gio.FileQueryInfoFlags.NONE, None)
		info = enumerator.next_file(None)

		while info:
			file = enumerator.get_child(info)
			installations.append(self._parseCustomInstallation(file.get_path()))
			info = enumerator.next_file(None)

		installations.sort(key = lambda i: i.priority)
		for i in range(len(installations)):
			installations[i] = installations[i]['path']
		return installations


	def _getInstallationsPaths(self):
		if self._paths:
			return self._paths

		self._paths = self._getCustomInstallationsPaths()
		self._paths.insert(0, self._getUserPath())
		self._paths.append(self._getSystemPath())

		return self._paths


	def _getBundlePathForAppId(self, appId):
		installations = self._getInstallationsPaths()
		for i in range(len(installations)):
			path = GLib.build_filenamev([installations[i], 'app', appId, 'current', 'active'])
			if GLib.access(path, 0) == 0:
				return path


	def _getIconThemePathForAppId(self, appId):
		return GLib.build_filenamev([self._getBundlePathForAppId(appId), 'export', 'share', 'icons'])



	# this only covers cases that follow the flathub convention
	def _isBaseApp(self, appId):
		return appId.endswith(".BaseApp")


	def _getApplicationsForPath(self, path):
		appList = []

		if GLib.access(path, 0) != 0:
			return appList

		directory = Gio.File.new_for_path(path)
		enumerator = directory.enumerate_children('*', Gio.FileQueryInfoFlags.NONE, None)
		info = enumerator.next_file(None)

		while info:
			file = enumerator.get_child(info)
			appId = GLib.path_get_basename(file.get_path())
			activePath = GLib.build_filenamev([file.get_path(), 'current', 'active'])

			if not self._isBaseApp(appId) and GLib.access(activePath, 0) == 0:
				appList.append(appId)

			info = enumerator.next_file(None)

		return appList


	def _getApproximateNameForAppId(self, appId):
		name = appId.split('.')[-1]
		return name.title()


	def getMetadataForAppId(self, appId):
		data = {"runtime": _('Unknown')}

		group = "Application"
		path = self.getMetadataPathForAppId(appId)

		if GLib.access(path, 0) != 0:
			return data

		keyFile = GLib.KeyFile()
		keyFile.load_from_file(path, 0)

		try:
			data['runtime'] = keyFile.get_value(group, 'runtime')
		except:
			data['runtime'] = ''

		return data


	def getDesktopForAppData(self, appdata):
		desktop = {"icon": "application-x-executable-symbolic"}

		path = GLib.build_filenamev([self._getBundlePathForAppId(appdata['appId']), 'export', 'share', 'applications', appdata['launchable']])

		if GLib.access(path, 0) != 0:
			return desktop

		app = AppStreamGlib.App()

		try:
			if not app.parse_file(path, AppStreamGlib.AppParseFlags.NONE):
				return desktop
		except:
			return desktop

		icon = app.get_icon_default()
		if icon is None:
			return desktop

		iconName = icon.get_name()
		if iconName:
			desktop['icon'] = iconName

		return desktop


	def getAppDataForAppId(self, appId):
		appdata = {"appId": appId, "name": self._getApproximateNameForAppId(appId), "author": _('Unknown'), "version": _('Unknown'), "date": _('Unknown'), "launchable": appId + '.desktop'}

		path = GLib.build_filenamev([self._getBundlePathForAppId(appId), 'files', 'share', 'appdata', appId + '.appdata.xml'])

		if GLib.access(path, 0) != 0:
			return appdata

		app = AppStreamGlib.App()

		try:
			if not app.parse_file(path, AppStreamGlib.AppParseFlags.NONE):
				return appdata
		except:
			return appdata

		name = app.get_name(None)
		if name:
			appdata['name'] = name

		author = app.get_developer_name(None)
		if author:
			appdata['author'] = author

		launchable = app.get_launchable_default()
		if launchable:
			val = launchable.get_value()
			if val:
				appdata['launchable'] = val

		release = app.get_release_default()
		if release is None:
			return appdata

		version = release.get_version()
		if version:
			appdata['version'] = version

		ts = release.get_timestamp()
		if ts:
			appdata['date'] = date.fromtimestamp(ts).isoformat()

		return appdata;

	def getAll(self):
		installations = self._getInstallationsPaths()
		applications = []

		for path in installations:
			app = GLib.build_filenamev([path, 'app'])
			applications.extend(self._getApplicationsForPath(app))

		applications.sort()

		appList = []
		for appId in applications:
			appdata = self.getAppDataForAppId(appId)
			desktop = self.getDesktopForAppData(appdata)

			appList.append({"appId": appId, "appThemePath": self._getIconThemePathForAppId(appId), "appName": appdata['name'], "appIconName": desktop['icon']})

		return appList

	def getMetadataPathForAppId(self, appId):
		return GLib.build_filenamev([self._getBundlePathForAppId(appId), 'metadata'])

