using Gtk 4.0;
using Adw 1;

template $PackageTransporterWindow : Adw.ApplicationWindow {
  default-width: 800;
  default-height: 800;
  width-request: 500;
  height-request: 500;
  title: "Package Transporter";

  Stack main_stack {
    transition-type: crossfade;

    StackPage {
      name: "begin_page";
      child: Box {
        orientation: vertical;

        Adw.HeaderBar {
          styles ["flat"]
        }

        Adw.StatusPage {
          icon-name: "io.gitlab.gwendalj.package-transporter";
          title: _("Ready to Transfer your Flatpaks ?");
          description: _("You can start backing up your installed applications and their configurations to recover them later.");
          hexpand: true;
          vexpand: true;
          Box {
            halign: center;
            Button btn_backup {
              name: "backup";
              label: "Start a Backup";
              margin-end: 30;

              styles ["pill", "suggested-action"]
            }
            Button btn_restore {
              name: "restore";
              label: "Restore an Install";

              styles ["pill"]
            }
          }
        }
      };
    }

    StackPage {
      name: "empty_page";
      child: Box {
        orientation: vertical;

        Adw.HeaderBar {
          styles ["flat"]
        }

        Adw.StatusPage {
          //icon-name: "grid-large-symbolic";
          title: _("No Flatpak applications found");
          hexpand: true;
          vexpand: true;
        }
      };
    }

    StackPage {
      name: "fake_page";
      child: Box {
        orientation: vertical;

        Adw.HeaderBar {
          styles ["flat"]
        }

        Adw.StatusPage {
          //icon-name: "grid-large-symbolic";
          title: _("Feature not yet implemented 🙃️");
          hexpand: true;
          vexpand: true;

          Box {
            halign: center;
            Button btn_home0 {
              name: "home";
              label: _("Back to Homepage");

              styles ["pill", "suggested-action"]
            }
          }
        }
      };
    }

    StackPage {
      name: "done_page";
      child: Box {
        orientation: vertical;

        Adw.HeaderBar {
          styles ["flat"]
        }

        Adw.StatusPage {
          //icon-name: "check-round-outline-symbolic";
          title: _("Well Done ! 👍️");
          description: _("Your backup has been successfully created.");
          hexpand: true;
          vexpand: true;

          Box {
            halign: center;
            Button btn_home1 {
              name: "home";
              label: _("Back to Homepage");

              styles ["pill", "suggested-action"]
            }
          }
        }
      };
    }

    StackPage {
      name: "loading_page";

      child: Box {
        orientation: vertical;

        Adw.HeaderBar {
          styles ["flat"]
        }

        Adw.Clamp {
          valign: center;

          styles [
            "status-page",
          ]

          Box {
            orientation: vertical;
            valign: center;

            Box {
              orientation: vertical;
              valign: center;
              halign: center;

              styles [
                "iconbox",
              ]

              Spinner loading_spinner {
                halign: center;
                valign: center;
                hexpand: true;
                vexpand: true;

                styles ["icon"]
              }
            }

            Label loading_title {
              wrap: true;
              wrap-mode: word_char;
              justify: center;

              styles ["title", "title-1"]
            }

            Label loading_desc {
              wrap: true;
              wrap-mode: word_char;
              justify: center;
              use-markup: true;

              styles ["body", "description"]
            }
          }
        }
      };
    }

    StackPage {
      name: "process_page";
      child:
      Box {
        orientation: vertical;

        Adw.HeaderBar {
          [start]
          Button btn_next {
            name: "next";
            label: "Next";

            styles ["suggested-action"]
          }

          [end]
          MenuButton appmenu_button {
            tooltip-text: _("Main Menu");
            primary: true;
            icon-name: "open-menu-symbolic";
            menu-model: primary_menu;
          }
        }

        Stack process_stack {
          transition-type: crossfade;

          StackPage {
            name: "list_page";
            child:
            ScrolledWindow {
              hexpand: true;
              vexpand: true;

              Adw.Clamp {
                maximum-size: 700;

                Box {
                  orientation: vertical;

                  Label list_page_title {
                    margin-top: 12;
                    margin-bottom: 24;
                    wrap: true;
                    styles ["title-2"]
                  }

                  Box {
                    orientation: horizontal;
                    margin-end: 24;

                    Label {
                      hexpand: true;
                      halign: end;
                      margin-end:12;
                      label: _('Select all');

                      styles ["title-3"]
                    }

                    CheckButton select_all {
                      active: true;
                    }
                  }

                  ListBox list_box {
                    valign: start;
                    margin-end: 12;
                    margin-start: 12;
                    margin-top: 12;
                    margin-bottom: 12;

                    styles ["boxed-list"]
                  }
                }
              }
            };
          }

        }
      };
    }

  }
}

menu primary_menu {
  section {
    item {
      label: _("_Preferences");
      action: "app.preferences";
    }

    item {
      label: _("_About Package Transporter");
      action: "app.about";
    }
  }
}
