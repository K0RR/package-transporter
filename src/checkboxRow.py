from gi.repository import GLib, Gtk, GObject, Adw

@Gtk.Template(resource_path='/io/gitlab/gwendalj/package-transporter/checkboxRow.ui')
class CheckboxActionRow(Adw.ActionRow):
    __gtype_name__ = "CheckboxActionRow"

    icon = Gtk.Template.Child()
    checkbox = Gtk.Template.Child()

    def __init__(self, title, subtitle, iconName, toggleCallback):
        super().__init__()
        self.icon.set_from_icon_name(iconName)
        self.set_title(GLib.markup_escape_text(title, -1))
        self.set_subtitle(GLib.markup_escape_text(subtitle, -1))
        if toggleCallback:
            self.checkbox.connect('toggled', toggleCallback)

    def active(self, state):
        if state is not None:
            self.checkbox.set_active(state)
        return self.checkbox.get_active()

    def setSubtitle(self, subtitle):
        self.set_subtitle(GLib.markup_escape_text(subtitle, -1))
