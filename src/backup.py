from gi.repository import GObject, GLib, Gio
from .filesutil import compressGzip
import json

# class FlatpakBackup(GObject.Object):
#   _appDataPath

#   def __init__(appDataPath):
#     self._appDataPath = appDataPath


#   def _copyAppToTmp(self, tmpPath, appId, saveData):
#     if GLib.mkdir_with_parents(GLib.build_filenamev([path, appId])) != 0:
#       return -1

#     tmpAppPath = GLib.build_filenamev([tmpPath, appId])
#     appPath = GLib.build_filenamev([self._appDataPath, appId])

#     #copy keyFile for appId
#     src = Gio.File.new_for_path(GLib.build_filenamev([appPath, 'config', 'glib-2.0', 'settings', 'keyfile'])
#     dest = Gio.File.new_for_path(GLib.build_filenamev([tmpAppPath, 'keyfile'])
#     src.copy(dest, Gio.FileCopyFlags.NONE, None, None, None)

#     return 0


def toJson(o):
  return o.__dict__

def backupSelection(appManager, destPath):
  appsPath = appManager._getApplicationsUserPath()

  # create file containing `data`
  jsonPath = GLib.build_filenamev([GLib.get_tmp_dir(), 'apps.json'])
  fos = Gio.File.new_for_path(jsonPath).create(Gio.FileCreateFlags.NONE, None)
  fos.write(json.dumps(appManager._applications, default=toJson).encode('utf-8'), None)
  fos.close(None)

  toBackup = {"apps.json": jsonPath}
  for app in appManager._applications:
    if app.save:
      keyFilePath = GLib.build_filenamev([appsPath, app.package, '/config/glib-2.0/settings/keyfile'])
      if GLib.access(keyFilePath, 0) == 0:
        toBackup[app.package+'/config/glib-2.0/settings/keyfile'] = keyFilePath
      if app.data:
        dataPath = GLib.build_filenamev([appsPath, app.package, '/data'])
        if GLib.access(dataPath, 0) == 0:
          toBackup[app.package+'/data'] = dataPath

  compressGzip(toBackup, destPath)

