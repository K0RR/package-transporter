from gi.repository import GObject, Gio, GLib
from .filesutil import do_recursively

class DirectoryScanner(GObject.Object):
  _asyncResult = None

  def _pruneFileTreeDepth(self, depth, resultDirectory):
    for subdir in resultDirectory["files"]:
      if "files" in subdir:
        if depth > 0:
          self._pruneFileTreeDepth(depth - 1, subdir)
        else:
          del subdir["files"]


  def scanner_regularFileCallback(self, info, name, result, userData):
    size = info.get_attribute_uint64(Gio.FILE_ATTRIBUTE_STANDARD_ALLOCATED_SIZE)
    if size == 0:
      size = info.get_size()
    if "files" not in result:
      result["files"] = []
    if "size" not in result:
      result["size"] = 0
    result["files"].append({"name": name, "size": size})
    result["size"] += size

  def scanner_directoryFileCallback(self, info, name, subResult, result, userData):
    subResult["name"] = name
    if "files" not in result:
      result["files"] = []
    if "size" not in result:
      result["size"] = 0
    if "size" in subResult:
      result["size"] += subResult["size"]

    result["files"].append(subResult)


  def getDirectoriesSize(self, path, depth, ignoreDirs):
    if GLib.access(path, 0) != 0:
      return None;

    data = do_recursively(Gio.File.new_for_path(path), ignoreDirs, self.scanner_regularFileCallback, self.scanner_directoryFileCallback, None)
    self._pruneFileTreeDepth(depth, data)

    self._asyncResult = data
    return data


  def getDirectoriesSize_async(self, path, depth, ignoreDirs, callback):
    task = Gio.Task.new(self, None, callback, None)
    task.run_in_thread(lambda task, src, task_data, cancellable: self.getDirectoriesSize(path, depth, ignoreDirs))

  def getDirectoriesSize_async_finish(self):
    return self._asyncResult

