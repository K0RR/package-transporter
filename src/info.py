# info.py
#
# Port made from Flatseal info.js
# by Martin Abente Lahaye
#

from gi.repository import GObject, GLib

class FlatpakInfoModel(GObject.Object):
	_version = None

	def __init__(self):
		super().__init__()

	def _getInfoPath(self):
		path = GLib.getenv('FLATPAK_INFO_PATH')
		if path:
			return path

		return GLib.build_filenamev([GLib.DIR_SEPARATOR_S, '.flatpak-info'])

	def _parseVersion(self):
		keyFile = GLib.KeyFile()
		try:
			keyFile.load_from_file(self._getInfoPath(), GLib.KeyFileFlags.NONE)
			return keyFile.get_value('Instance', 'flatpak-version')
		except:
			return None

	def getVersion(self):
		if self._version is None:
			self._version = self._parseVersion()

		return self._version



	def supports(self, target):
		version = self.getVersion()

		if version is None:
			return True

		versions = version.split('.')
		targets = target.split('.')
		components = max(len(versions), len(targets))

		for i in range(components):
			ver = int(versions[i])
			tar = int(targets[i])

			if ver < tar:
				return False
			if ver > tar:
				return True;

		return True

	def reload(self):
		self._version = self._parseVersion()

