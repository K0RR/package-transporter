# window.py
#
# Copyright 2023 Gwendal Janssoone
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, Gtk, GObject, GLib
from .checkboxRow import CheckboxActionRow
from .application import Application, ApplicationManager
from .applications import FlatpakApplicationsModel
from .backup import backupSelection


@Gtk.Template(resource_path='/io/gitlab/gwendalj/package-transporter/window.ui')
class PackageTransporterWindow(Adw.ApplicationWindow):
  __gtype_name__ = 'PackageTransporterWindow'

  main_stack = Gtk.Template.Child()
  btn_backup = Gtk.Template.Child()
  btn_restore = Gtk.Template.Child()
  btn_next = Gtk.Template.Child()
  btn_home0 = Gtk.Template.Child()
  btn_home1 = Gtk.Template.Child()

  process_stack = Gtk.Template.Child()
  loading_title = Gtk.Template.Child()
  loading_desc = Gtk.Template.Child()
  loading_spinner = Gtk.Template.Child()
  select_all = Gtk.Template.Child()
  list_box = Gtk.Template.Child()
  list_page_title = Gtk.Template.Child()

  step = 1

  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.main_stack.set_visible_child_name('begin_page')
    self.btn_backup.connect('clicked', self.on_startBackup)
    self.btn_restore.connect('clicked', self.on_startRestore)
    self.select_all.connect('toggled', self.on_selectAll)
    self.btn_next.connect('clicked', self.on_nextStep)
    self.btn_home0.connect('clicked', self.on_homeClicked)
    self.btn_home1.connect('clicked', self.on_homeClicked)

    self.applicationManager = ApplicationManager()


  def on_startBackup(self, button):
    flatpakApps = FlatpakApplicationsModel()
    appList = flatpakApps.getAll()
    if len(appList) == 0:
      self.main_stack.set_visible_child_name('empty_page')
    else:
      iconTheme = Gtk.IconTheme.get_for_display(self.get_display())
      for app in appList:
        iconTheme.add_search_path(app['appThemePath'])
        row = CheckboxActionRow(app['appName'], app['appId'], app['appIconName'], self.on_rowCheckbox)
        row.id = self.applicationManager.addApplication(Application(app['appName'], app['appId']))
        self.list_box.append(row)

      self.main_stack.set_visible_child_name('process_page')
      self.process_stack.set_visible_child_name('list_page')
      self.list_page_title.set_label(_("Step 1 : Choose which apps you want to backup"))


  def on_startRestore(self, button):
    self.main_stack.set_visible_child_name('fake_page')

  def on_homeClicked(self, button):
    self.main_stack.set_visible_child_name('begin_page')

  def on_selectAll(self, checkbox):
    i = 0
    while row := self.list_box.get_row_at_index(i):
      row.active(checkbox.get_active())
      i += 1
    checkbox.set_inconsistent(False)


  def on_rowCheckbox(self, checkbox):
    self.select_all.set_inconsistent(True)


  def on_nextStep(self, button):
    if self.step == 1:
      self.removeUncheckedFromListAndSave()
      rowCount = self.applicationManager.loadDataSizesForSaved_async(self.on_applicationSizeFinished)
      if rowCount > 10:
        self.startLoading(_("Computing applications sizes..."), _("This could take up to few minutes depending of the number of applications."))
      self.step += 1
    elif self.step == 2:
      self._native = Gtk.FileChooserNative(title="Save Backup File", transient_for=self, action=Gtk.FileChooserAction.SAVE, accept_label="_Save", cancel_label="_Cancel")
      ff = Gtk.FileFilter()
      ff.add_pattern("*.tar.gz")
      self._native.add_filter(ff)
      self._native.set_current_name("flatpaks.tar.gz")
      self._native.connect("response", self.on_saveBackupResponse)
      self._native.show()


  def removeUncheckedFromListAndSave(self):
    i = 0
    while row := self.list_box.get_row_at_index(i):
      self.applicationManager.get(row.id).save = row.active(None)
      if not row.active(None):
        self.list_box.remove(row)
        del row
      else:
        i += 1


  def on_saveBackupResponse(self, native, response):
    if response == Gtk.ResponseType.ACCEPT:
      self.startLoading(_('Generating backup package...'), _('This could take a while.'))

      i = 0
      while row := self.list_box.get_row_at_index(i):
        self.applicationManager.get(row.id).data = row.active(None)
        i += 1
      backupSelection(self.applicationManager, native.get_file().get_path())
      self.finishLoading("done_page")
    self._native = None


  def startLoading(self, title, description):
    self.loading_title.set_label(title)
    self.loading_desc.set_label(description)
    self.loading_spinner.start()
    self.main_stack.set_visible_child_name('loading_page')


  def finishLoading(self, nextPageMainStack):
    self.loading_spinner.stop()
    self.main_stack.set_visible_child_name(nextPageMainStack)


  def on_applicationSizeFinished(self):
    self.select_all.set_inconsistent(False)
    self.select_all.set_active(False)
    i = 0
    while row := self.list_box.get_row_at_index(i):
      row.active(False)
      row.setSubtitle(_("data folder size : ") + self.applicationManager.get(row.id).getDataSizeString())
      i += 1

    self.finishLoading("process_page")
    self.list_page_title.set_label(_("Step 2 : Choose if you want to also save apps data. (application settings are always saved)"))
    self.btn_next.set_label(_("Backup"))
