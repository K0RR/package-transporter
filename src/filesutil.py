from gi.repository import GLib, Gio
import tarfile

ATTRIBUTES = Gio.FILE_ATTRIBUTE_STANDARD_NAME + "," + Gio.FILE_ATTRIBUTE_STANDARD_TYPE + "," + Gio.FILE_ATTRIBUTE_STANDARD_SIZE +  "," + Gio.FILE_ATTRIBUTE_STANDARD_ALLOCATED_SIZE + "," + Gio.FILE_ATTRIBUTE_UNIX_NLINK + "," + Gio.FILE_ATTRIBUTE_UNIX_INODE + "," + Gio.FILE_ATTRIBUTE_ACCESS_CAN_READ

def do_recursively(directory, ignoreDirs, regularFileCallback, directoryFileCallback, userData):
  result = {}
  #hardlinks = []

  enumerator = directory.enumerate_children(ATTRIBUTES, Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS , None)

  while info := enumerator.next_file(None):
    if info.get_attribute_boolean(Gio.FILE_ATTRIBUTE_ACCESS_CAN_READ):
      name = info.get_name()
      if info.get_file_type() == Gio.FileType.REGULAR:

        if info.has_attribute(Gio.FILE_ATTRIBUTE_UNIX_NLINK):
          if info.get_attribute_uint32(Gio.FILE_ATTRIBUTE_UNIX_NLINK) > 1:
            continue
            #inode = info.get_attribute_uint64(Gio.FILE_ATTRIBUTE_UNIX_INODE)
            #if inode in hardlinks:
              #continue
            #hardlinks.append(inode)

        regularFileCallback(info, name, result, userData)

      elif info.get_file_type() == Gio.FileType.DIRECTORY and name not in ignoreDirs:
        subResult = do_recursively(enumerator.get_child(info), ignoreDirs, regularFileCallback, directoryFileCallback, userData)
        directoryFileCallback(info, name, subResult, result, userData)

  return result




def compressGzip(files, dest):
  with tarfile.open(dest, "w:gz") as tar:
    for arcname, name in files.items():
      tar.add(name, arcname)

