# application.py
#
# Copyright 2023 Gwendal Janssoone
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from .scanner import DirectoryScanner

class Application:
	def __init__(self, name, package):
		self.name = name
		self.package = package
		self.dataSize = -1

	def getDataSizeString(self):
		if self.dataSize != -1:
			return GLib.format_size(self.dataSize)
		else:
			return 'Unknown'


class ApplicationManager:
	_applications = []

	def _getApplicationsUserPath(self):
		path = GLib.build_filenamev([GLib.get_home_dir(), '.var', 'app'])
		return path

	
	def addApplication(self, application):
		self._applications.append(application)
		return len(self._applications) - 1


	def get(self, i):
		return self._applications[i]


	def getByPackage(self, package):
		for app in self._applications:
			if app.package == package:
				return app
		return None


	def loadDataSizesForSaved_async(self, finished_callback):
		def callback(source_object, res, data):
			result = source_object.getDirectoriesSize_async_finish()
			for r in result["files"]:
				if app := self.getByPackage(r["name"]):
					app.dataSize = r["size"]

			finished_callback()


		count = 0
		ignoreDirs = ["cache", ".ld.so", ".local"]
		for app in self._applications:
			if app.save:
				count += 1
			else:
				ignoreDirs.append(app.package)
		
		sc = DirectoryScanner()
		sc.getDirectoriesSize_async(self._getApplicationsUserPath(), 1, ignoreDirs, callback)

		return count

